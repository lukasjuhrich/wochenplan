import { StatusBar } from 'expo-status-bar';
import { Text, View } from 'react-native';
import * as rn from 'react-native';
import * as r from 'react';

const range = n => [...Array(n).keys()]

function foo() {
  console.log("Hallo, Jasmin :)")
}
export default function App() {
  const [x, setX] = r.useState(0);
  function addX() {
    console.log(x)
    setX(x => x + 1)
  }

  // TODO: want focus:shadow-inner focus:bg-orange-200, but these things don't work…

  return (
    <View className="_h-screen">
      <StatusBar style="auto" />

      <Header />

      <FlexList>
        <View className="
            flex-row justify-center
            m-3 px-5 py-2 bg-blue-200 rounded-lg shadow-xl shadow-cyan-500 px-0 py-0 
            ">
          <rn.TouchableOpacity onPress={addX} className="">
            <Text className="pl-5 pr-2 py-2 text-lg text-blue-900">Test. Value: {x}</Text>
          </rn.TouchableOpacity>
          <rn.TouchableOpacity onPress={() => setX(_ => 0)}
            className="pl-2 pr-3 py-2 bg-blue-300 inline-flex justify-center rounded-r-lg">
            <Text className="text-sm text-blue-900 font-bold">RST</Text>
          </rn.TouchableOpacity>
        </View>
        {range(5).map(i => (
          <MyButton title={`Ich bin button nr. ${i}`} onPress={foo} key={i} />
        ))}
      </FlexList>

    </View>
  );
}

function Header() {
  return (
    <View className="
        flex-block flex-col items-center
        font-medium 
        max-h-fit w-full pb-5 mb-10 pt-12 pb-8 text-4xl 
        color-cyan-900 bg-cyan-100
      ">
      <Text className="block w-full text-center text-4xl tracking-widest">Welcome to</Text>
      <Text className="block w-full text-center text-5xl tracking-normal">Wochenplan.</Text>
    </View>
  )
}

function MyButton({ title, textProps, ...props }) {
  return (
    <rn.TouchableOpacity className="m-3 px-5 py-2 bg-blue-200 rounded-lg shadow-xl shadow-cyan-500" {...props}>
      <Text className="text-lg text-blue-900" {...textProps}>{title}</Text>
    </rn.TouchableOpacity>
  )
}

function FlexList({children, ...props}) {
  return (
    <View className="flex-col h-auto grow justify-center items-center" {...props}>
      {children}
    </View>
  )
}
